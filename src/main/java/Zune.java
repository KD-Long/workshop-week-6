package org.bitbucket.zune;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Zune player
 */
public class Zune {

  /**   Logger for this class */
  static Logger logger = LoggerFactory.getLogger(Zune.class);


  /**
   *    A named Zune player
   *
   *    @param ownerName The name of the lucky owner
   */
  public Zune(String ownerName) {
    if ( ownerName != "" )
      deviceOwner = ownerName;
    else
      deviceOwner = "Bill";
  }

  /**   A anonymous Zune player. Belongs to Bill G. */
  public Zune() {
    deviceOwner = "Bill";
  }

  /** Owner of the device */
  public String deviceOwner;

  /**   Origin of time for player clock is 1st January 1980 */
  static final int ORIGIN_YEAR = 1980;

  /**
   *    Determine leap year status
   *
   *    @param y    The year to Check
   *    @return     true if and only if y is a leap year
   */
  private static boolean isLeapYear(int y) {
    //  Leap year iff (multiple of 4 but NOT multiple of 100) or multiple of 400
    return ( y % 4 == 0 && ( y / 100 ) * 100 != y ) || ( y % 400 == 1 );
    //##### note to self i have changed !=y
  }

  /**
   *    Compute the current year from the number of days elapsed since 1/1/1980
   */
  public static int computeYear(int days) {
    int year = ORIGIN_YEAR;
    System.out.println("days:   "+days/365.0+"     ####################");
    // While there is more than 365 days, add year
    while ( days > 365 ) {
      if ( isLeapYear(year) ) {
        // year is leap year
        if ( days > 366 ) {
          days -= 366;
          year += 1;
        } else{ //this happens only when the leap year is not complete (half way through a leap year)
          days =0;
          year += 1;
        }
      } else {
        // year is not leap year
        days -= 365;
        year += 1;
      }
    }
    return year;
  }
}
