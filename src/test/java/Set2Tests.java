package org.bitbucket.zune;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.junit.Ignore;
import org.junit.runners.Suite;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;




/**
 * Test suite for encoding a string
 */
@RunWith( Parameterized.class )
public class Set2Tests {
  /** Limit each test to 10 millis */

  /**
   *  The collection of tests as an Array of String x String x byte[]
   */
  /* *INDENT-OFF* */
  @Parameters
  public static Collection<Object[]> data() {
    // testcases will de displayed as test[0], test[1] and so on
    return Arrays.asList(new Object[][] {
         { 1980, 330 },{ 1982, 930},{1981,630},{1980,365}
    });
  }
  /* *INDENT-ON* */

  // Test parameters
  // @link{https://github.com/junit-team/junit4/wiki/Parameterized-tests}
  private int val1;
  private int val2;


  /**
   *  Constructor for the tests
   */
  public Set2Tests(int v1, int v2) {
    val1  = v1;
    val2  = v2;
  }

  private Zune z = new Zune("foo");

  /**
   * Run all the tests in data()
   */
  @Test
  public void test()  {
    assertEquals(val1, z.computeYear(val2));
  }
}
